package runners;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\Users\\admin\\Desktop\\PR-Form-Sg\\AppiumTest\\src\\test\\resources\\Function\\Featurefile.feature",
        tags = {"@Android","~@ignore"},
        glue= {"stepDefinitions"},
        plugin = {"pretty", "html:target/site/cucumber-pretty",
                "json:reports/cucumber/cucumber.json",
                "junit:reports/newrelic-report/cucumber-junit.xml"}

)
public class TestRunner {
}