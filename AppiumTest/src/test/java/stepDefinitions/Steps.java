package stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;

public class Steps {

    private AppiumDriver<? extends MobileElement> driver;
    private AppiumDriverContainer appiumDriverContainer;



    @Given("^I launch the app$")
    public void i_launch_the_app() throws Throwable {
        File classpathRoot = new File(System.getProperty("user.dir"));
        File app = new File(classpathRoot, "Storms.apk");
        DesiredCapabilities androidCapabilities = new DesiredCapabilities();
        System.out.println(app.getAbsolutePath());
        androidCapabilities.setCapability("deviceName", "52107fc8ba918421");
        androidCapabilities.setCapability("automationName", "UiAutomator2");
        androidCapabilities.setCapability("platformName", "Android");
        androidCapabilities.setCapability("platformVersion", "8.1");
        androidCapabilities.setCapability("app", app.getAbsolutePath());
        androidCapabilities.setCapability("appPackage", "com.storms.store");
        androidCapabilities.setCapability("appActivity", "com.stormsclient.MainActivity");
        androidCapabilities.setCapability("noReset", true);
        androidCapabilities.setCapability("fullReset", false);
        androidCapabilities.setCapability("ignoreHiddenApiPolicyError", true);
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), androidCapabilities);
        WebDriverWait wait = new WebDriverWait(driver, 90);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//android.widget.TextView[@text='News'])[1]/preceding::android.view.ViewGroup[1]")));

    }

    @And("^I enter Username as \"([^\"]*)\" and Password as \"([^\"]*)\", validate the message$")
    public void iEnterUsernameAndPassword(String username, String password) throws Throwable {
        driver.findElementByXPath("(//android.widget.EditText)[1]").sendKeys(username);
        driver.findElementByXPath("(//android.widget.EditText)[1]").sendKeys(password);
        driver.findElementByXPath("//android.widget.TextView[@text='STORM IN']").click();
        if(driver.getPageSource().contains("Username or password is empty"))
        {
            System.out.println("User name and password is empty");
        }
        else if(driver.getPageSource().contains("PreAuthentication failed"))
        {
            System.out.println("username and password is invalid");
        }
        else
        {
            System.out.println("Login sucess");
        }
    }

    @When("^I click on Hamburger Menu$")
    public void iClickOnHamburgerMenu() throws Throwable {
        if (driver.findElementByXPath("(//android.widget.TextView[@text='News'])[1]/preceding::android.view.ViewGroup[1]").isEnabled()) {
            driver.findElementByXPath("(//android.widget.TextView[@text='News'])[1]/preceding::android.view.ViewGroup[1]").click();
        }
    }

    @Then("^I click on login button$")
    public void i_validate_the_hamburger_options() throws Throwable {
        if (driver.findElementByXPath("//android.widget.Button[@index='1']").isEnabled()) {
            driver.findElementByXPath("//android.widget.Button[@index='1']").click();
        }
    }

    @Then("^I validate the Login In text$")
    public void i_validate_the_login_in_text() throws Throwable {
        Assert.assertEquals(driver.findElementByXPath("//android.view.View").getText(),"Login in");
    }
    @Then("^I validate the Hello Gamer$")
    public void i_validate_the_HelloGamer_text() throws Throwable {
        Assert.assertEquals(driver.findElementByXPath("(//android.widget.TextView)[1]").getText(),"Hello, Gamer");
    }

    @Then("^I validate the forgot password$")
    public void i_validate_the_forgot_password() throws Throwable {
        Assert.assertEquals(driver.findElementByXPath("(//android.widget.TextView)[2]").getText(),"Forgot your password?");
    }

    @Then("^I validate the interested in storms$")
    public void i_validate_the_interested_in_storms() throws Throwable {
        Assert.assertEquals(driver.findElementByXPath("(//android.widget.TextView)[4]").getText(),"Interested in Storms? Join");
    }

    @Then("^I validate the bottom news, games and account is present$")
    public void i_validate_the_bottom_news_games_and_account_is_present() throws Throwable {
        Assert.assertEquals(driver.findElementByXPath("//android.widget.Button[@content-desc=\"News, tab, 1 of 3\"]").getText(),"News");
        Assert.assertEquals(driver.findElementByXPath("//android.widget.Button[@content-desc=\"Games, tab, 2 of 3\"]").getText(),"Games");
        Assert.assertEquals(driver.findElementByXPath("//android.widget.Button[@content-desc=\"Account, tab, 3 of 3\"]").getText(),"Account");
    }

}
