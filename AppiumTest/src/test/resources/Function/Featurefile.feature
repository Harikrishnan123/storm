@Android
Feature: Verify the Storm application



  @login
  Scenario Outline: Login to the Storm Application
    Given I launch the app
    When I click on Hamburger Menu
    Then I click on login button
    Then I validate the Login In text
    Then I validate the Hello Gamer
    Then I validate the forgot password
    Then I validate the interested in storms
    Then I validate the bottom news, games and account is present
    And I enter Username as "<username>" and Password as "<password>", validate the message

    Examples:
      | username  | password  |
      | username1 | password1 |
      | username2 | Test      |
      |           |           |
      |  username2|           |
      |           | Test      |
      | !@#$%^&*()|           |
      | !@#$%^&*()| password1 |
      | !@#$%^&*()| !@#$%^&*( |
      | 82850815  | password1 |
      | +6582850815| password1|
      | +6582850815| 67676    |
      | +6582850815| 67676    |